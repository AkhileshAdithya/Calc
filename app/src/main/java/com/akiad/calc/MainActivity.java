package com.akiad.calc;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
        Float a = 0f;
        Float b = 0f;
        Integer operator = 0;
        Float ans;
        //operator 1=add; 2=sub, 3=multiply, 4=divide

        Button button0,buttonCdup, button1,button2,button3,button4,button5,button6,button7,button8,button9,buttonpoint,buttonplus,buttonminus,buttonx,buttonslash,buttonC,buttonequal;
        TextView text1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text1 = findViewById(R.id.textbox);
        button0 = findViewById(R.id.but0);
        button1 = findViewById(R.id.but1);
        button2 = findViewById(R.id.but2);
        button3 = findViewById(R.id.but3);
        button4 = findViewById(R.id.but4);
        button5 = findViewById(R.id.but5);
        button6 = findViewById(R.id.but6);
        button7 = findViewById(R.id.but7);
        button8 = findViewById(R.id.but8);
        button9 = findViewById(R.id.but9);
        buttonCdup = findViewById(R.id.butCdup);
        buttonequal = findViewById(R.id.butequal);
        buttonpoint = findViewById(R.id.butpoint);
        buttonplus = findViewById(R.id.butplus);
        buttonminus = findViewById(R.id.butminus);
        buttonx = findViewById(R.id.butx);
        buttonslash = findViewById(R.id.butslash);
        buttonC = findViewById(R.id.butC);
        button0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text1.setText(text1.getText() + "0");
            }
        });
        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text1.setText("");
            }
        });
        buttonCdup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text1.setText("");
            }
        });
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text1.setText(text1.getText() + "1");
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text1.setText(text1.getText() + "2");
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text1.setText(text1.getText() + "3");
            }
        });
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text1.setText(text1.getText() + "4");
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text1.setText(text1.getText() + "5");
            }
        });
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text1.setText(text1.getText() + "6");
            }
        });
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text1.setText(text1.getText() + "7");
            }
        });
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text1.setText(text1.getText() + "8");
            }
        });
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text1.setText(text1.getText() + "9");
            }
        });
        buttonpoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                text1.setText(text1.getText()+ ".");
            }
        });
        buttonplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                a = Float.parseFloat(text1.getText().toString());
                operator = 1;
                text1.setText("");

            }
        });
        buttonminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                a = Float.parseFloat(text1.getText().toString());
                operator = 2;
                text1.setText("");

            }
        });
        buttonx.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                a = Float.parseFloat(text1.getText().toString());
                operator = 3;
                text1.setText("");

            }
        });
        buttonslash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                a = Float.parseFloat(text1.getText().toString());
                operator = 4;
                text1.setText("");

            }
        });
        buttonequal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b = Float.parseFloat(text1.getText().toString());
                if(operator == 1){
                    ans = a+b;
                    text1.setText(Float.toString(ans));
                }
                if(operator == 2){
                    ans = a-b;
                    text1.setText(Float.toString(ans));
                }
                if(operator == 3){
                    ans = a*b;
                    text1.setText(Float.toString(ans));
                }
                if(operator == 4){
                    ans = a/b;
                    text1.setText(Float.toString(ans));
                }



            }
        });





    }
}
